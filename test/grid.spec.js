const { expect } = require('chai');
const { Grid, cellsToFrame } = require('../src/grid.js');

describe('Grid', () => {
  describe('.prototype', () => {
    describe('.getCells', () => {
      it('should return a list of cells matching the specified parameters', () => {
        const frame = { x: 1300, y: -500, width: 1080, height: 1920 };
        const dimensions = { x: 3, y: 4 };
        const margins = 5;
        const expected = [
          {
            position: { x: 0, y: 0 },
            topLeft: { x: 1305, y: -495 },
            bottomRight: { x: 1657, y: -23 },
            width: 353,
            height: 473
          },
          {
            position: { x: 2, y: 3},
            topLeft: { x: 2021, y: 939 },
            bottomRight: { x: 2373, y: 1411 },
            width: 353,
            height: 473
          }
        ];
        const actual = new Grid({ frame, dimensions, margins }).getCells();
        expect(actual).to.containSubset(expected.slice(0, 1));
        expect(actual).to.containSubset(expected.slice(1, 1));
      });

      it('should work correctly when margins are 0', () => {
        const frame = { x: 0, y: 0, width: 100, height: 100 };
        const dimensions = { x: 5, y: 5 };
        const margins = 0;
        const expected = [
          {
            position: { x: 0, y: 0 },
            topLeft: { x: 0, y: 0 },
            bottomRight: { x: 19, y: 19 },
            width: 20,
            height: 20
          },
          {
            position: { x: 4, y: 4 },
            topLeft: { x: 80, y: 80 },
            bottomRight: { x: 99, y: 99 },
            width: 20,
            height: 20
          }
        ];
        const actual = new Grid({ frame, dimensions, margins }).getCells();
        expect(actual).to.containSubset(expected.slice(0, 1));
        expect(actual).to.containSubset(expected.slice(1, 1));
      });
    });

    describe('.getContainingCells', () => {
      it('should return the list of cells overlapped by >0.5 width or height', () => {
        const parentFrame = { x: 0, y: 0, width: 100, height: 100 };
        const dimensions = { x: 5, y: 5 };
        const margins = 0;
        const grid = new Grid({ frame: parentFrame, dimensions, margins });
        const winFrame = { x: 35, y: 5, width: 30, height: 50 };
        const expected = [
          { position: { x: 2, y: 0 }, topLeft: { x: 40, y: 0 }, bottomRight: { x: 59, y: 19 }, width: 20, height: 20 },
          { position: { x: 2, y: 1 }, topLeft: { x: 40, y: 20 }, bottomRight: { x: 59, y: 39 }, width: 20, height: 20 },
          { position: { x: 2, y: 2 }, topLeft: { x: 40, y: 40 }, bottomRight: { x: 59, y: 59 }, width: 20, height: 20 }
        ];
        const actual = grid.getContainingCells(winFrame);
        expected.forEach(expected => expect(actual).to.containSubset([expected]));
        expect(actual.length).to.equal(3);
      });
    });

    describe('.shiftCellsLeft', () => {
      const allCells = [
        { position: { x: 0, y: 0 } },
        { position: { x: 0, y: 1 } },
        { position: { x: 1, y: 0 } },
        { position: { x: 1, y: 1 } },
        { position: { x: 2, y: 0 } },
        { position: { x: 2, y: 1 } }
      ];

      describe('when there are cells to the left', () => {
        it('should return the set of cells shifted to the left', () => {
          const selected = allCells.slice(2, 6);
          const expected = allCells.slice(0, 4);
          const grid = new Grid();
          grid.getCells = () => allCells;
          const actual = grid.shiftCellsLeft(selected);
          expect(actual).to.deep.equal(expected);
        });
      });

      describe('when there are no cells to the left', () => {
        it('should return the set of cells provided', () => {
          const selected = allCells.slice(0, 4);
          const expected = allCells.slice(0, 4);
          const grid = new Grid();
          grid.getCells = () => allCells;
          const actual = grid.shiftCellsLeft(selected);
          expect(actual).to.deep.equal(expected);
        });
      });
    });

    describe('.shiftCellsRight', () => {
      const allCells = [
        { position: { x: 0, y: 0 } },
        { position: { x: 0, y: 1 } },
        { position: { x: 1, y: 0 } },
        { position: { x: 1, y: 1 } },
        { position: { x: 2, y: 0 } },
        { position: { x: 2, y: 1 } }
      ];

      describe('when there are cells to the right', () => {
        it('should return the set of cells shifted to the right', () => {
          const selected = allCells.slice(0, 4);
          const expected = allCells.slice(2, 6);
          const grid = new Grid();
          grid.getCells = () => allCells;
          const actual = grid.shiftCellsRight(selected);
          expect(actual).to.deep.equal(expected);
        });
      });

      describe('when there are no cells to the right', () => {
        it('should return the set of cells provided', () => {
          const selected = allCells.slice(2, 6);
          const expected = allCells.slice(2, 6);
          const grid = new Grid();
          grid.getCells = () => allCells;
          const actual = grid.shiftCellsRight(selected);
          expect(actual).to.deep.equal(expected);
        });
      });
    });

    describe('.shiftCellsUp', () => {
      const allCells = [
        { position: { x: 0, y: 0 } },
        { position: { x: 1, y: 0 } },
        { position: { x: 0, y: 1 } },
        { position: { x: 1, y: 1 } },
        { position: { x: 0, y: 2 } },
        { position: { x: 1, y: 2 } }
      ];

      describe('when there are cells above', () => {
        it('should return the set of cells shifted up', () => {
          const selected = allCells.slice(2, 4);
          const expected = allCells.slice(0, 2);
          const grid = new Grid();
          grid.getCells = () => allCells;
          const actual = grid.shiftCellsUp(selected);
          expect(actual).to.deep.equal(expected);
        });
      });

      describe('when there are no cells above', () => {
        it('should return the set of cells provided', () => {
          const selected = allCells.slice(0, 2);
          const expected = allCells.slice(0, 2);
          const grid = new Grid();
          grid.getCells = () => allCells;
          const actual = grid.shiftCellsUp(selected);
          expect(actual).to.deep.equal(expected);
        });
      });
    });

    describe('.shiftCellsDown', () => {
      const allCells = [
        { position: { x: 0, y: 0 } },
        { position: { x: 1, y: 0 } },
        { position: { x: 0, y: 1 } },
        { position: { x: 1, y: 1 } },
        { position: { x: 0, y: 2 } },
        { position: { x: 1, y: 2 } }
      ];

      describe('when there are cells below', () => {
        it('should return the set of cells shifted down', () => {
          const selected = allCells.slice(2, 4);
          const expected = allCells.slice(4, 6);
          const grid = new Grid();
          grid.getCells = () => allCells;
          const actual = grid.shiftCellsDown(selected);
          expect(actual).to.deep.equal(expected);
        });
      });

      describe('when there are no cells below', () => {
        it('should return the set of cells provided', () => {
          const selected = allCells.slice(4, 6);
          const expected = allCells.slice(4, 6);
          const grid = new Grid();
          grid.getCells = () => allCells;
          const actual = grid.shiftCellsDown(selected);
          expect(actual).to.deep.equal(expected);
        });
      });
    });

    [ { x: 'x', y: 'y', width: 'width', column: 'column' }, { x: 'y', y: 'x', width: 'height', column: 'row' } ]
      .forEach(({ x, y, width, column }) => {
        describe(`.shrinkCells${x.toUpperCase()}`, () => {
          describe(`when given set of cells has a ${width} of >1`, () => {
            const cells = [
              { position: { x: 0, y: 0 } },
              { position: { x: 0, y: 1 } },
              { position: { x: 1, y: 0 } },
              { position: { x: 1, y: 1 } }
            ];

            it(`should reduce the cells by one ${column}`, () => {
              const expected = [ { [x]: 0, [y]: 0 }, { [x]: 0, [y]: 1 } ].map(position => ({ position }));
              const actual = (new Grid())[`shrinkCells${x.toUpperCase()}`](cells);
              expect(actual).to.deep.equal(expected);
            });
          });

          describe(`when given set of cells has a ${width} of 1`, () => {
            it('should return the given set of cells', () => {
              const expected = [ { position: { [x]: 1, [y]: 4 } } ];
              const actual = (new Grid())[`shrinkCells${x.toUpperCase()}`](expected);
              expect(actual).to.deep.equal(expected);
            });
          });
        });

        describe(`.expandCells${x.toUpperCase()}`, () => {
          const allCells = [...Array(3).keys()]
            .map(x => [...Array(3).keys()].map(y => ({ x, y })))
            .reduce((items, item) => items.concat(item), [])
            .map(position => ({ position }));

          describe(`when there is another ${column} available`, () => {
            const grid = new Grid();
            grid.getCells = () => allCells;

            it(`should add one ${column} to the selected cells`, () => {
              const cells = allCells.filter(({ position }) => position[x] < 2);
              const expected = allCells;
              const actual = grid[`expandCells${x.toUpperCase()}`](cells);
              expect(actual).to.deep.equal(expected);
            });
          });

          describe(`when there isn't another ${column} available`, () => {
            const grid = new Grid();
            grid.getCells = () => allCells;

            it(`should return the given set of cells unchanged`, () => {
              const expected = allCells;
              const actual = grid[`expandCells${x.toUpperCase()}`](allCells);
              expect(actual).to.deep.equal(expected);
            });
          });
        });
      });

    describe('.getLeftHalfCells', () => {
      const cells = [
        { position: { x: 0, y: 0 } },
        { position: { x: 1, y: 0 } },
        { position: { x: 2, y: 0 } },
        { position: { x: 0, y: 1 } },
        { position: { x: 1, y: 1 } },
        { position: { x: 2, y: 1 } },
        { position: { x: 0, y: 2 } },
        { position: { x: 1, y: 2 } },
        { position: { x: 2, y: 2 } }
      ];

      it('should return the left half of the cells', () => {
        const expected = cells.filter(({ position: { x } }) => x === 0);
        const grid = new Grid();
        grid.getCells = () => cells;
        const actual = grid.getLeftHalfCells();
        expect(actual).to.deep.equal(expected);
      });
    });

    describe('.getRightHalfCells', () => {
      const cells = [
        { position: { x: 0, y: 0 } },
        { position: { x: 1, y: 0 } },
        { position: { x: 2, y: 0 } },
        { position: { x: 0, y: 1 } },
        { position: { x: 1, y: 1 } },
        { position: { x: 2, y: 1 } },
        { position: { x: 0, y: 2 } },
        { position: { x: 1, y: 2 } },
        { position: { x: 2, y: 2 } }
      ];

      it('should return the left half of the cells', () => {
        const expected = cells.filter(({ position: { x } }) => x === 2);
        const grid = new Grid();
        grid.getCells = () => cells;
        const actual = grid.getRightHalfCells();
        expect(actual).to.deep.equal(expected);
      });
    });

    describe('.getTopHalfCells', () => {
      const cells = [
        { position: { x: 0, y: 0 } },
        { position: { x: 0, y: 1 } },
        { position: { x: 0, y: 2 } },
        { position: { x: 1, y: 0 } },
        { position: { x: 1, y: 1 } },
        { position: { x: 1, y: 2 } },
        { position: { x: 2, y: 0 } },
        { position: { x: 2, y: 1 } },
        { position: { x: 2, y: 2 } }
      ];

      it('should return the top half of the cells', () => {
        const expected = cells.filter(({ position: { y } }) => y === 0);
        const grid = new Grid();
        grid.getCells = () => cells;
        const actual = grid.getTopHalfCells();
        expect(actual).to.deep.equal(expected);
      });
    });

    describe('.getBottomHalfCells', () => {
      const cells = [
        { position: { x: 0, y: 0 } },
        { position: { x: 0, y: 1 } },
        { position: { x: 0, y: 2 } },
        { position: { x: 1, y: 0 } },
        { position: { x: 1, y: 1 } },
        { position: { x: 1, y: 2 } },
        { position: { x: 2, y: 0 } },
        { position: { x: 2, y: 1 } },
        { position: { x: 2, y: 2 } }
      ];

      it('should return the bottom half of the cells', () => {
        const expected = cells.filter(({ position: { y } }) => y === 2);
        const grid = new Grid();
        grid.getCells = () => cells;
        const actual = grid.getBottomHalfCells();
        expect(actual).to.deep.equal(expected);
      });
    });

    describe('.getCenterCells', () => {
      it('should return a set of cells for each x and y dimension between 1 and 16 (inclusive)', () => {
        const testCells = [...Array(16).keys()].map(x => [...Array(16).keys()].map(y => ({ x, y })))
          .reduce((items, item) => items.concat(item), [])
          .map(position => ({ position }));
        const grid = new Grid();
        for (let x = 0; x < 16; x++) {
          for (let y = 0; y < 16; y++) {
            grid.getCells = () => testCells.filter(({ position }) => position.x <= x && position.y <= y);
            const centerCells = grid.getCenterCells();
            const msgCtx = `given dimensions of ${x+1}x${y+1}`;
            expect(centerCells, `No result ${msgCtx}`).to.be.ok;
            expect(centerCells.length).to.be.above(0, `Empty array of cells ${msgCtx}`);
          }
        }
      });
    });
  });
});

describe('cellsToFrame', () => {
  it('should return a frame combining the specified cells', () => {
    const cells = [
      { topLeft: { x: 100, y: 200 }, bottomRight: { x: 199, y: 299 } },
      { topLeft: { x: 200, y: 300 }, bottomRight: { x: 299, y: 399 } }
    ];
    const expected = { x: 100, y: 200, width: 200, height: 200 };
    const actual = cellsToFrame(cells);
    expect(actual).to.deep.equal(expected);
  });
});

