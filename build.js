const path = require('path');

const modConcat = require('node-module-concat');

const outputFile = path.join(__dirname, 'phoenix.js');

modConcat(path.join(__dirname, './src/main.js'), outputFile, function(err, stats) {
    if (err) throw err;
    console.log(stats.files.length + ' were combined into ' + outputFile);
});
