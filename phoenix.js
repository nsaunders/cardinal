/* This header is placed at the beginning of the output file and defines the
	special `__require`, `__getFilename`, and `__getDirname` functions.
*/
(function() {
	/* __modules is an Array of functions; each function is a module added
		to the project */
var __modules = {},
	/* __modulesCache is an Array of cached modules, much like
		`require.cache`.  Once a module is executed, it is cached. */
	__modulesCache = {},
	/* __moduleIsCached - an Array of booleans, `true` if module is cached. */
	__moduleIsCached = {};
/* If the module with the specified `uid` is cached, return it;
	otherwise, execute and cache it first. */
function __require(uid, parentUid) {
	if(!__moduleIsCached[uid]) {
		// Populate the cache initially with an empty `exports` Object
		__modulesCache[uid] = {"exports": {}, "loaded": false};
		__moduleIsCached[uid] = true;
		if(uid === 0 && typeof require === "function") {
			require.main = __modulesCache[0];
		} else {
			__modulesCache[uid].parent = __modulesCache[parentUid];
		}
		/* Note: if this module requires itself, or if its depenedencies
			require it, they will only see an empty Object for now */
		// Now load the module
		__modules[uid](__modulesCache[uid], __modulesCache[uid].exports);
		__modulesCache[uid].loaded = true;
	}
	return __modulesCache[uid].exports;
}
/* This function is the replacement for all `__filename` references within a
	project file.  The idea is to return the correct `__filename` as if the
	file was not concatenated at all.  Therefore, we should return the
	filename relative to the output file's path.

	`path` is the path relative to the output file's path at the time the
	project file was concatenated and added to the output file.
*/
function __getFilename(path) {
	return require("path").resolve(__dirname + "/" + path);
}
/* Same deal as __getFilename.
	`path` is the path relative to the output file's path at the time the
	project file was concatenated and added to the output file.
*/
function __getDirname(path) {
	return require("path").resolve(__dirname + "/" + path + "/../");
}
/********** End of header **********/
/********** Start module 0: /Users/nsaunders/Projects/scratch/cardinal/src/main.js **********/
__modules[0] = function(module, exports) {
const { Grid, cellsToFrame } = __require(1,0);

const GRID_PARAMS = { margins: 10, dimensions: { x: 4, y: 4 } };
const HYPER = ['ctrl', 'alt', 'cmd'];
const SHIFT_HYPER = ['shift'].concat(HYPER);

const createGrid = frame => new Grid(Object.assign({ frame }, GRID_PARAMS));
const getScreen = win => win.screen().flippedVisibleFrame();
const getNextScreen = win => (win.screen().next() || win.screen()).flippedVisibleFrame();
const vertical = ({ width, height }) => width < height;

const translate = fn => {
  return () => {
    const win = Window.focused();
    if (win) {
      const screen = getScreen(win);
      const grid = createGrid(screen);
      const cells = grid.getContainingCells(win.frame());
      win.setFrame(cellsToFrame(fn({ grid, cells, screen })));
    }
  };
};

const move = selectCells => {
  return getScreen => {
    return () => {
      const win = Window.focused();
      if (win) {
        const screen = getScreen(win);
        const grid = createGrid(screen);
        win.setFrame(cellsToFrame(selectCells({ screen, grid })));
      }
    };
  };
};

const translateLeft = translate(({ grid, cells }) => grid.shiftCellsLeft(cells));
const translateRight = translate(({ grid, cells }) => grid.shiftCellsRight(cells));
const translateUp = translate(({ grid, cells }) => grid.shiftCellsUp(cells));
const translateDown = translate(({ grid, cells }) => grid.shiftCellsDown(cells));

const shrinkX = translate(({ grid, cells }) => grid.shrinkCellsX(cells));
const shrinkY = translate(({ grid, cells }) => grid.shrinkCellsY(cells));
const expandX = translate(({ grid, cells }) => grid.expandCellsX(cells));
const expandY = translate(({ grid, cells }) => grid.expandCellsY(cells));

const maximize = move(({ grid }) => grid.getCells());
const center = move(({ grid }) => grid.getCenterCells());

const halve1 = move(({ grid, screen }) => vertical(screen) ? grid.getTopHalfCells() : grid.getLeftHalfCells());
const halve1R = move(({ grid, screen }) => !vertical(screen) ? grid.getTopHalfCells() : grid.getLeftHalfCells());
const halve2 = move(({ grid, screen }) => vertical(screen) ? grid.getBottomHalfCells() : grid.getRightHalfCells());
const halve2R = move(({ grid, screen }) => !vertical(screen) ? grid.getBottomHalfCells() : grid.getRightHalfCells());

Key.on('h', HYPER, translateLeft);
Key.on('j', HYPER, translateDown);
Key.on('k', HYPER, translateUp);
Key.on('l', HYPER, translateRight);
Key.on('h', SHIFT_HYPER, shrinkX);
Key.on('j', SHIFT_HYPER, expandY);
Key.on('k', SHIFT_HYPER, shrinkY);
Key.on('l', SHIFT_HYPER, expandX);
Key.on(';', HYPER, center(getScreen));
Key.on("'", HYPER, maximize(getScreen));
Key.on('w', HYPER, halve1(getScreen));
Key.on('w', SHIFT_HYPER, halve1R(getScreen));
Key.on('e', HYPER, halve2(getScreen));
Key.on('e', SHIFT_HYPER, halve2R(getScreen));
Key.on('n', HYPER, center(getNextScreen));

return module.exports;
}
/********** End of module 0: /Users/nsaunders/Projects/scratch/cardinal/src/main.js **********/
/********** Start module 1: /Users/nsaunders/Projects/scratch/cardinal/src/grid.js **********/
__modules[1] = function(module, exports) {
exports.Grid = class _ {
  constructor(params) {
    this._params = params;
    if (params) {
      ['x', 'y'].forEach(k => {
        if (params[k] < 1 || params[k] > 16) {
          throw new Error('Grid dimensions must be between 1 and 16!');
        }
      });
    }
  }

  getCells() {
    const { frame, dimensions, margins } = this._params;
    const cells = [];
    const width = Math.floor((frame.width - margins * (dimensions.x + 1)) / dimensions.x);
    const height = Math.floor((frame.height - margins * (dimensions.y + 1)) / dimensions.y);
    const padX = 0, padY = 0; // Use for centering grid within frame later on
    for (let x = 0; x < dimensions.x; x++) {
      for (let y = 0; y < dimensions.y; y++) {
        let cell = {
          position: { x, y },
          topLeft: { x: padX + frame.x + x * width + (x + 1) * margins, y: padY + frame.y + y * height + (y + 1) * margins },
          width,
          height
        };
        cell.bottomRight = { x: cell.topLeft.x + cell.width - 1, y: cell.topLeft.y + cell.height - 1 };
        cells.push(cell);
      }
    }
    return cells;
  }

  getContainingCells(frame) {
    const frameBR = { x: frame.x + frame.width - 1, y: frame.y + frame.height - 1 };
    return this
      .getCells()
      .filter(cell => {
        const mid = { x: cell.topLeft.x + cell.width / 2, y: cell.topLeft.y + cell.height / 2 };
        const coversX = frame.x <= cell.topLeft.x && frameBR.x > cell.bottomRight.x;
        const coversY = frame.y <= cell.topLeft.y && frameBR.y > cell.bottomRight.y;
        const coversAll = coversX && coversY;
        const coversLeft = frame.x <= cell.topLeft.x && frameBR.x > mid.x;
        const coversTop = frame.y <= cell.topLeft.y && frameBR.y > mid.y;
        const coversRight = frame.x <= mid.x && frameBR.x > cell.bottomRight.x;
        const coversBottom = frame.y <= mid.y && frameBR.y > cell.bottomRight.y;
        const coversSide = (coversTop && (coversLeft || coversRight)) || (coversBottom && (coversLeft || coversRight));
        return coversAll || coversSide;
      });
  }

  shiftCellsLeft(cells) {
    const allCells = this.getCells();
    const newCells = cells
      .map(({ position: p1 }) => allCells.find(({ position: p2 }) => p2.x === p1.x - 1 && p2.y === p1.y))
      .filter(cell => cell);
    return newCells.length === cells.length ? newCells : cells;
  }

  shiftCellsRight(cells) {
    const allCells = this.getCells();
    const newCells = cells
      .map(({ position: p1 }) => allCells.find(({ position: p2 }) => p2.x === p1.x + 1 && p2.y === p1.y))
      .filter(cell => cell);
    return newCells.length === cells.length ? newCells : cells;
  }

  shiftCellsUp(cells) {
    const allCells = this.getCells();
    const newCells = cells
      .map(({ position: p1 }) => allCells.find(({ position: p2 }) => p2.y === p1.y - 1 && p2.x === p1.x))
      .filter(cell => cell);
    return newCells.length === cells.length ? newCells : cells;
  }

  shiftCellsDown(cells) {
    const allCells = this.getCells();
    const newCells = cells
      .map(({ position: p1 }) => allCells.find(({ position: p2 }) => p2.y === p1.y + 1 && p2.x === p1.x))
      .filter(cell => cell);
    return newCells.length === cells.length ? newCells : cells;
  }

  shrinkCellsX(cells) {
    const xs = cells.map(({ position: { x } }) => x);
    const min = Math.min.apply(Math, xs);
    const max = Math.max.apply(Math, xs);
    return min === max ? cells : cells.filter(({ position: { x } }) => x < max);
  }

  expandCellsX(cells) {
    const xs = cells.map(({ position: { x } }) => x);
    const ys = cells.map(({ position: { y } }) => y);
    const minX = Math.min.apply(Math, xs);
    const maxX = Math.max.apply(Math, xs) + 1;
    const minY = Math.min.apply(Math, ys);
    const maxY = Math.max.apply(Math, ys);
    return this
      .getCells()
      .filter(({ position: { x, y } }) => x >= minX && y >= minY && x <= maxX && y <= maxY);
  }

  shrinkCellsY(cells) {
    const ys = cells.map(({ position: { y } }) => y);
    const min = Math.min.apply(Math, ys);
    const max = Math.max.apply(Math, ys);
    return min === max ? cells : cells.filter(({ position: { y } }) => y < max);
  }

  expandCellsY(cells) {
    const xs = cells.map(({ position: { x } }) => x);
    const ys = cells.map(({ position: { y } }) => y);
    const minX = Math.min.apply(Math, xs);
    const maxX = Math.max.apply(Math, xs);
    const minY = Math.min.apply(Math, ys);
    const maxY = Math.max.apply(Math, ys) + 1;
    return this
      .getCells()
      .filter(({ position: { x, y } }) => x >= minX && y >= minY && x <= maxX && y <= maxY);
  }

  getColumns() {
    return Math.max.apply(Math, this.getCells().map(cell => cell.position.x)) + 1;
  }

  getRows() {
    return Math.max.apply(Math, this.getCells().map(cell => cell.position.y)) + 1;
  }

  getLeftHalfCells() {
    const columns = this.getColumns();
    return this.getCells().filter(({ position: { x } }) => x <= columns / 2 - 1);
  }

  getRightHalfCells() {
    const columns = this.getColumns();
    return this.getCells().filter(({ position: { x } }) => x >= columns / 2);
  }

  getTopHalfCells() {
    const rows = this.getRows();
    return this.getCells().filter(({ position: { y } }) => y <= rows / 2 - 1);
  }

  getBottomHalfCells() {
    const rows = this.getRows();
    return this.getCells().filter(({ position: { y } }) => y >= rows / 2);
  }

  getCenterCells() {
    const selection = {
      1: [0],
      2: [0, 1],
      3: [1],
      4: [1, 2],
      5: [1, 2, 3],
      6: [1, 2, 3, 4],
      7: [2, 3, 4],
      8: [2, 3, 4, 5],
      9: [2, 3, 4, 5, 6],
      10: [2, 3, 4, 5, 6, 7],
      11: [3, 4, 5, 6, 7],
      12: [3, 4, 5, 6, 7, 8],
      13: [3, 4, 5, 6, 7, 8, 9],
      14: [3, 4, 5, 6, 7, 8, 9, 10],
      15: [3, 4, 5, 6, 7, 8, 9, 10, 11],
      16: [4, 5, 6, 7, 8, 9, 10, 11]
    };
    const xs = selection[this.getColumns()];
    const ys = selection[this.getRows()];

    if (!xs || !ys) return [];

    return this
      .getCells()
      .filter(({ position: { x, y } }) => xs.includes(x) && ys.includes(y));
  }
};

exports.cellsToFrame = cells => {
  const x = Math.min.apply(Math, cells.map(c => c.topLeft.x));
  const y = Math.min.apply(Math, cells.map(c => c.topLeft.y));
  const width = Math.max.apply(Math, cells.map(c => c.bottomRight.x)) - x + 1;
  const height = Math.max.apply(Math, cells.map(c => c.bottomRight.y)) - y + 1;
  return { x, y, width, height };
};


return module.exports;
}
/********** End of module 1: /Users/nsaunders/Projects/scratch/cardinal/src/grid.js **********/
/********** Footer **********/
if(typeof module === "object")
	module.exports = __require(0);
else
	return __require(0);
})();
/********** End of footer **********/
