exports.Grid = class _ {
  constructor(params) {
    this._params = params;
    if (params) {
      ['x', 'y'].forEach(k => {
        if (params[k] < 1 || params[k] > 16) {
          throw new Error('Grid dimensions must be between 1 and 16!');
        }
      });
    }
  }

  getCells() {
    const { frame, dimensions, margins } = this._params;
    const cells = [];
    const width = Math.floor((frame.width - margins * (dimensions.x + 1)) / dimensions.x);
    const height = Math.floor((frame.height - margins * (dimensions.y + 1)) / dimensions.y);
    const padX = 0, padY = 0; // Use for centering grid within frame later on
    for (let x = 0; x < dimensions.x; x++) {
      for (let y = 0; y < dimensions.y; y++) {
        let cell = {
          position: { x, y },
          topLeft: { x: padX + frame.x + x * width + (x + 1) * margins, y: padY + frame.y + y * height + (y + 1) * margins },
          width,
          height
        };
        cell.bottomRight = { x: cell.topLeft.x + cell.width - 1, y: cell.topLeft.y + cell.height - 1 };
        cells.push(cell);
      }
    }
    return cells;
  }

  getContainingCells(frame) {
    const frameBR = { x: frame.x + frame.width - 1, y: frame.y + frame.height - 1 };
    return this
      .getCells()
      .filter(cell => {
        const mid = { x: cell.topLeft.x + cell.width / 2, y: cell.topLeft.y + cell.height / 2 };
        const coversX = frame.x <= cell.topLeft.x && frameBR.x > cell.bottomRight.x;
        const coversY = frame.y <= cell.topLeft.y && frameBR.y > cell.bottomRight.y;
        const coversAll = coversX && coversY;
        const coversLeft = frame.x <= cell.topLeft.x && frameBR.x > mid.x;
        const coversTop = frame.y <= cell.topLeft.y && frameBR.y > mid.y;
        const coversRight = frame.x <= mid.x && frameBR.x > cell.bottomRight.x;
        const coversBottom = frame.y <= mid.y && frameBR.y > cell.bottomRight.y;
        const coversSide = (coversTop && (coversLeft || coversRight)) || (coversBottom && (coversLeft || coversRight));
        return coversAll || coversSide;
      });
  }

  shiftCellsLeft(cells) {
    const allCells = this.getCells();
    const newCells = cells
      .map(({ position: p1 }) => allCells.find(({ position: p2 }) => p2.x === p1.x - 1 && p2.y === p1.y))
      .filter(cell => cell);
    return newCells.length === cells.length ? newCells : cells;
  }

  shiftCellsRight(cells) {
    const allCells = this.getCells();
    const newCells = cells
      .map(({ position: p1 }) => allCells.find(({ position: p2 }) => p2.x === p1.x + 1 && p2.y === p1.y))
      .filter(cell => cell);
    return newCells.length === cells.length ? newCells : cells;
  }

  shiftCellsUp(cells) {
    const allCells = this.getCells();
    const newCells = cells
      .map(({ position: p1 }) => allCells.find(({ position: p2 }) => p2.y === p1.y - 1 && p2.x === p1.x))
      .filter(cell => cell);
    return newCells.length === cells.length ? newCells : cells;
  }

  shiftCellsDown(cells) {
    const allCells = this.getCells();
    const newCells = cells
      .map(({ position: p1 }) => allCells.find(({ position: p2 }) => p2.y === p1.y + 1 && p2.x === p1.x))
      .filter(cell => cell);
    return newCells.length === cells.length ? newCells : cells;
  }

  shrinkCellsX(cells) {
    const xs = cells.map(({ position: { x } }) => x);
    const min = Math.min.apply(Math, xs);
    const max = Math.max.apply(Math, xs);
    return min === max ? cells : cells.filter(({ position: { x } }) => x < max);
  }

  expandCellsX(cells) {
    const xs = cells.map(({ position: { x } }) => x);
    const ys = cells.map(({ position: { y } }) => y);
    const minX = Math.min.apply(Math, xs);
    const maxX = Math.max.apply(Math, xs) + 1;
    const minY = Math.min.apply(Math, ys);
    const maxY = Math.max.apply(Math, ys);
    return this
      .getCells()
      .filter(({ position: { x, y } }) => x >= minX && y >= minY && x <= maxX && y <= maxY);
  }

  shrinkCellsY(cells) {
    const ys = cells.map(({ position: { y } }) => y);
    const min = Math.min.apply(Math, ys);
    const max = Math.max.apply(Math, ys);
    return min === max ? cells : cells.filter(({ position: { y } }) => y < max);
  }

  expandCellsY(cells) {
    const xs = cells.map(({ position: { x } }) => x);
    const ys = cells.map(({ position: { y } }) => y);
    const minX = Math.min.apply(Math, xs);
    const maxX = Math.max.apply(Math, xs);
    const minY = Math.min.apply(Math, ys);
    const maxY = Math.max.apply(Math, ys) + 1;
    return this
      .getCells()
      .filter(({ position: { x, y } }) => x >= minX && y >= minY && x <= maxX && y <= maxY);
  }

  getColumns() {
    return Math.max.apply(Math, this.getCells().map(cell => cell.position.x)) + 1;
  }

  getRows() {
    return Math.max.apply(Math, this.getCells().map(cell => cell.position.y)) + 1;
  }

  getLeftHalfCells() {
    const columns = this.getColumns();
    return this.getCells().filter(({ position: { x } }) => x <= columns / 2 - 1);
  }

  getRightHalfCells() {
    const columns = this.getColumns();
    return this.getCells().filter(({ position: { x } }) => x >= columns / 2);
  }

  getTopHalfCells() {
    const rows = this.getRows();
    return this.getCells().filter(({ position: { y } }) => y <= rows / 2 - 1);
  }

  getBottomHalfCells() {
    const rows = this.getRows();
    return this.getCells().filter(({ position: { y } }) => y >= rows / 2);
  }

  getCenterCells() {
    const selection = {
      1: [0],
      2: [0, 1],
      3: [1],
      4: [1, 2],
      5: [1, 2, 3],
      6: [1, 2, 3, 4],
      7: [2, 3, 4],
      8: [2, 3, 4, 5],
      9: [2, 3, 4, 5, 6],
      10: [2, 3, 4, 5, 6, 7],
      11: [3, 4, 5, 6, 7],
      12: [3, 4, 5, 6, 7, 8],
      13: [3, 4, 5, 6, 7, 8, 9],
      14: [3, 4, 5, 6, 7, 8, 9, 10],
      15: [3, 4, 5, 6, 7, 8, 9, 10, 11],
      16: [4, 5, 6, 7, 8, 9, 10, 11]
    };
    const xs = selection[this.getColumns()];
    const ys = selection[this.getRows()];

    if (!xs || !ys) return [];

    return this
      .getCells()
      .filter(({ position: { x, y } }) => xs.includes(x) && ys.includes(y));
  }
};

exports.cellsToFrame = cells => {
  const x = Math.min.apply(Math, cells.map(c => c.topLeft.x));
  const y = Math.min.apply(Math, cells.map(c => c.topLeft.y));
  const width = Math.max.apply(Math, cells.map(c => c.bottomRight.x)) - x + 1;
  const height = Math.max.apply(Math, cells.map(c => c.bottomRight.y)) - y + 1;
  return { x, y, width, height };
};

