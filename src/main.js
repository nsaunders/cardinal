const { Grid, cellsToFrame } = require('./grid.js');

const GRID_PARAMS = { margins: 10, dimensions: { x: 4, y: 4 } };
const HYPER = ['ctrl', 'alt', 'cmd'];
const SHIFT_HYPER = ['shift'].concat(HYPER);

const createGrid = frame => new Grid(Object.assign({ frame }, GRID_PARAMS));
const getScreen = win => win.screen().flippedVisibleFrame();
const getNextScreen = win => (win.screen().next() || win.screen()).flippedVisibleFrame();
const vertical = ({ width, height }) => width < height;

const translate = fn => {
  return () => {
    const win = Window.focused();
    if (win) {
      const screen = getScreen(win);
      const grid = createGrid(screen);
      const cells = grid.getContainingCells(win.frame());
      win.setFrame(cellsToFrame(fn({ grid, cells, screen })));
    }
  };
};

const move = selectCells => {
  return getScreen => {
    return () => {
      const win = Window.focused();
      if (win) {
        const screen = getScreen(win);
        const grid = createGrid(screen);
        win.setFrame(cellsToFrame(selectCells({ screen, grid })));
      }
    };
  };
};

const translateLeft = translate(({ grid, cells }) => grid.shiftCellsLeft(cells));
const translateRight = translate(({ grid, cells }) => grid.shiftCellsRight(cells));
const translateUp = translate(({ grid, cells }) => grid.shiftCellsUp(cells));
const translateDown = translate(({ grid, cells }) => grid.shiftCellsDown(cells));

const shrinkX = translate(({ grid, cells }) => grid.shrinkCellsX(cells));
const shrinkY = translate(({ grid, cells }) => grid.shrinkCellsY(cells));
const expandX = translate(({ grid, cells }) => grid.expandCellsX(cells));
const expandY = translate(({ grid, cells }) => grid.expandCellsY(cells));

const maximize = move(({ grid }) => grid.getCells());
const center = move(({ grid }) => grid.getCenterCells());

const halve1 = move(({ grid, screen }) => vertical(screen) ? grid.getTopHalfCells() : grid.getLeftHalfCells());
const halve1R = move(({ grid, screen }) => !vertical(screen) ? grid.getTopHalfCells() : grid.getLeftHalfCells());
const halve2 = move(({ grid, screen }) => vertical(screen) ? grid.getBottomHalfCells() : grid.getRightHalfCells());
const halve2R = move(({ grid, screen }) => !vertical(screen) ? grid.getBottomHalfCells() : grid.getRightHalfCells());

Key.on('h', HYPER, translateLeft);
Key.on('j', HYPER, translateDown);
Key.on('k', HYPER, translateUp);
Key.on('l', HYPER, translateRight);
Key.on('h', SHIFT_HYPER, shrinkX);
Key.on('j', SHIFT_HYPER, expandY);
Key.on('k', SHIFT_HYPER, shrinkY);
Key.on('l', SHIFT_HYPER, expandX);
Key.on(';', HYPER, center(getScreen));
Key.on("'", HYPER, maximize(getScreen));
Key.on('w', HYPER, halve1(getScreen));
Key.on('w', SHIFT_HYPER, halve1R(getScreen));
Key.on('e', HYPER, halve2(getScreen));
Key.on('e', SHIFT_HYPER, halve2R(getScreen));
Key.on('n', HYPER, center(getNextScreen));
